# Custom params for docker-compose up
PARAMS=

# ------------------------ Promtail ------------------------ #
.PHONY: run-silent run-silent run stop rm shell clean

run-silent: PARAMS:=-d ${PARAMS}
run-silent: run

dry-run: ENV:=CMD="-dry-run" ${ENV}
dry-run: run

run:
	${ENV} docker-compose up ${PARAMS}

stop: 
	${ENV} docker-compose stop

rm:
	@printf "This action will delete containers and all associated data.\nAre you sure (y/N)?" && read choice && [ $${choice:-N} = y ] &&\
	${ENV} docker-compose down -v

shell:
	@docker-compose exec -u root promtail /bin/bash
	@true

.IGNORE: rm
clean: rm
